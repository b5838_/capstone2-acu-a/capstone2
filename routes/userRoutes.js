const express = require("express")
const router = express.Router();

// controllers
const userControllers = require("../controllers/userControllers")

// Authentication
const auth = require("../auth")

// User Registration
router.post("/register", (req, res) => {       
    userControllers.registerUser(req.body).then(result => res.send(result));
})

// User login
router.post("/login", (req, res) => {
    userControllers.loginUser(req.body).then(result => res.send(result));
})

// Get a specific Profile
router.get("/profile", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    userControllers.getProfile(userData.id).then(result => res.send(result))
})

// Get all Users (Only admin is allowed to access this path)
router.get("/all", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin){
        userControllers.getAllUsers().then(allUser => res.send(allUser))
    }
    else{
        res.send({message: "You cannot access this path"})
    }
})

// My Pending Orders
router.get("/myOrders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin){
        res.send({message: "You dont have permission to access this path"})
    }
    userControllers.myPendingOrders(userData.id).then(result => res.send(result));
})

// My Completed Orders
router.get("/myCompleteOrders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin){
        res.send({message: "You dont have permission to access this path"})
    }
    userControllers.myCompletedOrders(userData.id).then(result => res.send(result));
})

// Reset Password
router.patch("/password", (req, res) => {
    userControllers.resetPassword(req.body).then(result => res.send(result))
})

// Set User as Admin
router.patch("/:userId", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin){
        userControllers.addAdmin(req.params.userId)
        .then(result => res.send(result))
    }
    else{
        res.send({message: "You cannot access this path"})
    }
})

module.exports = router;