const express = require("express");
const router = express.Router();

// Controllers
const orderControllers = require("../controllers/orderControllers")

// Authentication
const auth = require("../auth");

// Create Order (users)
router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    const data = {
        userId: userData.id,
        productId: req.body.productId
    }

    if(userData.isAdmin){
        res.send({message: "You cannot place an order"});
    }
    else{
        orderControllers.createOrder(data, req.body)
        .then(order => res.send(order));
    }
})

// Get all orders (admin)
router.get("/all", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        orderControllers.getAllOrders()
        .then(order => res.send(order));
    }
    else{
        res.send({message: "You dont have permission to access this path"});
    }
})

// Update status to complete (admin)
router.patch("/:orderId", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        orderControllers.updateStatusToComplete(req.params.orderId)
        .then(status => res.send(status));
    }
    else{
        res.send({message: "You dont have permission to access this path"});
    }
})

// Get all pending orders (admin)
router.get("/pendingOrders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        orderControllers.getAllPending()
        .then(pending => res.send(pending));
    }
    else{
        res.send({message: "You dont have permission to access this path"});
    }
})

// Get all completed orders (admin)
router.get("/completeOrders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        orderControllers.getAllComplete()
        .then(complete => res.send(complete));
    }
    else{
        res.send({message: "You dont have permission to access this path"});
    }
})

// Get order of specific userid (admin)
router.get("/userOrder/:userId", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        orderControllers.getOrderOfUser(req.params.userId)
        .then(userOrder => res.send(userOrder));
    }
    else{
        res.send({message: "You dont have permission to access this path"});
    }
})


module.exports = router;