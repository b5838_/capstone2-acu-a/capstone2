const express = require("express")
const router = express.Router();

// Controllers
const productControllers = require("../controllers/productControllers");

// Authentication
const auth = require("../auth")

// Create Product(admin)
router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        productControllers.addProduct(req.body)
        .then(productCreated => res.send(productCreated));
    }
    else{
        res.send({message: "You cannot create a product"});
    }
})

// Get all product (admin)
router.get("/allproducts", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        productControllers.getAllProducts()
        .then(allProduct => res.send(allProduct))
    }
    else{
        res.send({message: "You cannot access this path"})
    }
})

// Get all active product (both)
router.get("/active", (req, res) => {
    productControllers.getActiveProducts()
    .then(activeProducts => res.send(activeProducts))
})

// Get a product (both)
router.get("/:productId", auth.verify, (req, res) => {
    
    const userData = auth.decode(req.headers.authorization);

    productControllers.getProduct(req.params.productId, userData)
    .then(product => res.send(product));
})

// Update a product (admin)
router.put("/:productId/update", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin){
        productControllers.updateProduct(req.params.productId, req.body)
        .then(update => res.send(update))
    }
    else{
        res.send({message: "You cannot access this path"})
    }
})

// Archive a product (admin)
router.patch("/:productId/archive", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin){
        productControllers.archiveProduct(req.params.productId)
        .then(archive => res.send(archive))
    }
    else{
        res.send({message: "You cannot access this path"})
    }
})


module.exports = router;