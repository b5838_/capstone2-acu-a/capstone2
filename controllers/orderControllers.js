// MODELS
const User = require("../models/User");
const Product = require("../models/Product")
const Order = require("../models/Order")

// Create Order (users)
module.exports.createOrder = async (data, reqBody) => {

    const product = await Product.findById(data.productId);
    const user = await User.findById(data.userId);

    if(product.stocks === 0){
        return {message: "Out of stock"};
    }
    else if(product.stocks < reqBody.quantity){
        return {message: `Only ${product.stocks} stocks are available`};
    }
    else{
        const orderCreated = await (() =>{
            const newOrder = new Order({
                userId: data.userId,
                username: user.username,
                productId: data.productId,
                productName: product.name,
                price: product.price,
                quantity: reqBody.quantity,
                totalAmount: product.price * reqBody.quantity
            });
    
            return newOrder.save().then((success, error) =>{
                if(error){ return false }
                else{ return true }
            });
        })();
    
        const isUserOrder = await User.findById(data.userId)
        .then(user => {
    
            const userOrder = {
                productId: data.productId,
                productName: product.name,
                quantity: reqBody.quantity,
                price: product.price,
                totalAmount: product.price * reqBody.quantity
            };
    
            user.purchaseLog.push(userOrder);
    
            return user.save().then((success, error) => {
                if(error){ return false }
                else{ return true }
            });
        });
    
        const isProductUpdated = await Product.findById(data.productId)
        .then(product => {
    
            const orders = {
                userId: data.userId,
                username: user.username,
                quantity: reqBody.quantity,
                totalAmount: product.price * reqBody.quantity
            };
    
            product.orderLog.push(orders);
    
            product.stocks -= reqBody.quantity;
    
            return product.save().then((success, error) => {
                if(error){ return false }
                else{ return true }
            });
        });
    
    
        if(isUserOrder && isProductUpdated && orderCreated){
            return {message: "Order created"};
        }
        else{
            return {message: "Order failed"};
        }
    }
};

// Get all orders (admin)
module.exports.getAllOrders = () => {
    return Order.find({}).then(orders => orders)
};

// Update status to complete (admin)
module.exports.updateStatusToComplete = (orderId) => {
   return Order.findByIdAndUpdate(orderId, {status: "Complete"})
   .then(result => {
        return {message: `Status Complete, orderId ${orderId}`}
   })
};

// Get all pending orders (admin)
module.exports.getAllPending = () => {
    return Order.find({status: "Pending"}).then(pending => pending)
}

// Get all completed orders (admin)
module.exports.getAllComplete = () => {
    return Order.find({status: "Complete"}).then(complete => complete)
}

// Get order of specific userid (dmin)
module.exports.getOrderOfUser = async (userId) => {
    return Order.find({userId: userId}).then(result => result)
}