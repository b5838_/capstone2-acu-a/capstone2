// model
const Product = require("../models/Product")

// Create Product (Admin)
module.exports.addProduct = (reqBody) => {
    const newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        stocks: reqBody.stocks
    })

    return newProduct.save().then((result, error) => {
        if(error) {return {message: "Failed to create a Product"}}
        else {return {message: "Product created"}}
    })
} 

// Get all product (admin)
module.exports.getAllProducts = () => {
    return Product.find({}).then(results => results)
}

// Get all active product (Both)
module.exports.getActiveProducts = () => {
    return Product.find(
        {isActive: true}, 
        {
            description: 0,
            orderLog: 0,
            createdOn: 0,
            __v: 0
        })
        .then(result => result)
}

// Get a product (both)
module.exports.getProduct = (productId, userData) => {
    if(userData.isAdmin){
        return Product.findById(productId).then(allProduct => allProduct)
    }
    else{
        return Product.findById(
            productId, 
            {
                orderLog: 0,
                __v: 0
            })
            .then(product => product)
    }
}

// Update a product (admin)
module.exports.updateProduct = (productId, reqBody) => {

    const newUpdatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        stocks: reqBody.stocks
    }

    return Product.findByIdAndUpdate(productId, newUpdatedProduct)
    .then((success, error) => {
        if(error){ return {message: "Failed to update"} }
        else{ return {message: "Product updated"} }
    })
}


// Archive a product (admin)
module.exports.archiveProduct = (productId) => {
    return Product.findByIdAndUpdate(productId, {isActive: false})
    .then((success, error) => {
        if(error){ return {message: "Failed to archive"} }
        else{ return {message: "Product archive"} }
    })
}
