// model
const User = require("../models/User");
const Order = require("../models/Order");

// encryption and authentication
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User registration
module.exports.registerUser = (reqBody) => {

    return User.findOne({email: reqBody.email})
    .then(user => {
        if(user !== null){
            return {message: `${reqBody.email} is already exist`}
        }
        else{
            const randomNumbers = Math.floor(Math.random()*99)

            const newUser = new User({
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                username: `@${reqBody.firstName.toLowerCase()}_${randomNumbers}`,
                mobileNo: reqBody.mobileNo,
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 9)
            })

            return newUser.save().then((result, error) => {
                if(error){ return false; }
                else{ return {message: "Registration successful"} }
            })
        }
    }) 
}        

// User login
module.exports.loginUser = (reqBody) => {
    
    return User.findOne({email: reqBody.email}).then((result) => {
        if(result == null){
            return {message: "Email Not found"};
        }
        else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            if(isPasswordCorrect){
               return {access: auth.createAccessToken(result)}
            }
            else{
                return {message: "Incorrect Password"};
            }
        }
    })
}

// Get a specific Profile
module.exports.getProfile = (userId) => {
    return User.findById(
        userId,
        {
            password: 0
        })
        .then(result => result)
}

// Get all Users (Only admin is allowed to access this path)
module.exports.getAllUsers = () => {
    return User.find(
        {},
        {
            password: 0
        })
        .then(results => results)
    
}

// My Pending Orders
module.exports.myPendingOrders = (userId) => {
    return Order.find(
        {$and: [{userId: userId}, {status: "Pending"}]},
        {
            userId: 0,
            username: 0
        })
        .then(result => result)
}

// My Completed Orders
module.exports.myCompletedOrders = (userId) => {
    return Order.find(
        {$and: [{userId: userId}, {status: "Complete"}]},
        {
            userId: 0,
            username: 0
        })
        .then(result => result)
}


// Reset Password
module.exports.resetPassword = (reqBody) => {

    const newPassword = {
        password: bcrypt.hashSync(reqBody.password, 9)
    }
    return User.findOneAndUpdate({email: reqBody.email}, newPassword).then((success, error) => {
        if(error){ return false }
        else{ return {message: "Password change successful"} }
    })
}

// Set User as Admin(Only Admin can set another Admin)
module.exports.addAdmin = (userId) => {

    return User.findById(userId).then(result => {
        if(result.isAdmin){
            return {message: `${result.username} is already an Admin`}
        }
        else{
            const newAdmin = {
                isAdmin: true
            }
            return User.findByIdAndUpdate(userId, newAdmin)
            .then((newAddedAdmin, error) =>{
                if(error){ return false}
                else{ return {message: `You make ${result.username} an Admin`} }
            })
        }
    })
}
