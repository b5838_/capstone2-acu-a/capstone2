const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, "User ID is required"]
    },
    username: {
        type: String,
        require: [true, "Username is required"]
    },
    productId: {
        type: String,
        required: [true, "Product ID is required"]
    },
    productName: {
        type: String,
        required: [true, "Product name is required"]
    },
    quantity: {
        type: Number,
        default: 1
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    totalAmount: {
        type: Number,
        required: [true, "Total Amount is required"]
    },
    orderedOn: {
        type: Date,
        default: new Date()
    },
    status: {
        type: String,
        default: "Pending"
    } 
})


module.exports = mongoose.model("Order", orderSchema)

    