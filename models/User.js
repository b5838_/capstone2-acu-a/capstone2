const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    
    firstName: {
        type: String,
        required: [true, "First name is required"]
    },
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    username: {
        type: String,
        required: [true, "Username is required"]
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile number is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    registeredOn: {
        type: Date,
        default: new Date()
    },
    purchaseLog: [
        {
            productId: {
                type: String,
                required: [true, "Product ID is required"]
            },
            productName: {
                type: String,
                required: [true, "Product name is required"]
            },
            quantity: {
                type: Number,
                default: 1
            },
            price: {
                type: Number,
                required: [true, "Price is required"]
            },
            totalAmount: {
                type: Number,
                required: [true, "Total Amount is required"]
            },
            orderDate: {
                type: Date,
                default: new Date(),
            }
        }
    ]

})


module.exports = mongoose.model("User", userSchema)