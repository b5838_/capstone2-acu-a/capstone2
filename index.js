// Require modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Require Routes
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")
const orderRoutes = require("./routes/orderRoutes")

// Create server
const app = express();
const port = 4000;

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@myfirstcluster.fm4c2.mongodb.net/ecommerce-app?retryWrites=true&w=majority", {
    useNewUrlParser: true, 
    useUnifiedTopology: true
})

// Set notification for connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("We're connected to the cloud database."));

// Middlewares
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Allow all resources to access our backend application
app.use("/users", userRoutes)
app.use("/products", productRoutes)
app.use("/orders", orderRoutes)

// Listening to port
app.listen(process.env.PORT || port, () => {
    console.log(`API is now online on port ${process.env.PORT || port}`);
})